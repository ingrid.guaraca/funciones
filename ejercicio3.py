#Desplaza la llamada de la función de nuevo hacia el final, y coloca la definición de muestra_estribillo después
# de la definición de repite_estribillo. ¿Qué ocurre cuando haces funcionar ese programa?
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"

# ejercicio normal
# def muestra_estribillo():
#    print('Soy un leñador, que alegría.')
#    print('Duermo toda la noche y trabajo todo el día.')
# def repite_estribillo():
#   muestra_estribillo()
#   muestra_estribillo()

# repite_estribillo()


# ejercicio realizado como pide el enunciado
print('Soy un leñador, que alegría.')
print('Duermo toda la noche y trabajo todo el día.')
def repite_estribillo():
    def muestra_estribillo():
        muestra_estribillo()
        muestra_estribillo()

repite_estribillo()

