#Desplaza la última línea del programa anterior hacia arriba,de modo que la llamada a la función aparezca antes
# que las definiciones. Ejecuta el programa y observa qué mensaje de error obtienes.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"

# ejercicio normal
# def muestra_estribillo():
#   print('Soy un leñador, que alegría.')
#   print('Duermo toda la noche y trabajo todo el día.')
# def repite_estribillo():
#  muestra_estribillo()
#  muestra_estribillo()

#repite_estribillo()


# ejercicio realizado como pide el enunciado
repite_estribillo()
def muestra_estribillo():
    print('Soy un leñador, que alegría.')
    print('Duermo toda la noche y trabajo todo el día.')
def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()
