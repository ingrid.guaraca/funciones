#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
#¿Qué mostrará en pantalla el siguiente programa Python?

def fred():
    print("Zap")
def jane():
    print("ABC")

jane()
fred()
jane()

# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
"d) ABC Zap ABC  /R/ respuesta correcta"""
# e) Zap Zap Zap