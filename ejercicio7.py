# Reescribe el programa de calificaciones del capítulo anterior usando una función llamada calcula_calificacion,
# que reciba una puntuación como parámetro y devuelva una calificación como cadena.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
try:
    puntuación = float (input("Introduzca puntuación: "))

    def calcula_calificación(puntuación):
        if puntuación >= 0.0 and puntuación <= 1.0:
            if puntuación >= 0.9:
                puntuación = "Sobresaliente"
            elif puntuación >= 0.8:
                puntuación = "Notable"
            elif puntuación >= 0.7:
                puntuación = "Bien"
            elif puntuación >= 0.6:
                puntuación = "Suficiente"
            elif puntuación < 6:
                puntuación = "Insuficiente"
        else:
             print ("Puntuación incorrecta")
        return puntuación
    print (calcula_calificación(puntuación))

except:
    print ("Puntuación incorrecta")





