# Reescribe el programa de cálculo del salario, con tarifa-ymedia para las horas extras, y crea una función llamada
# calculo_salario que reciba dos parámetros (horas y tarifa).
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"

horas=int(input("Introduzca horas: "))
tarifa=float(input("Introduzca tarifa: "))

def calculo_salario(horas,tarifa):
    if horas > 40:
        hrsextra = horas-40
        salextra = hrsextra * 1.5 * tarifa
        salario= tarifa *40 + salextra
        salario= "Salario ", salario
    else:
        salario= horas *tarifa
        salario = ("Salario ", salario)
    return salario

print (calculo_salario(horas,tarifa))
